---
title: "About"
date: 2018-02-06T11:07:05-06:00
draft: false
menu: ["main", "about"]
featured: true
color: "color-brand-alt-bg"
weight: "1"
image: "/images/students.jpg"
icon: "graduation-cap"
subtitle: "Your world is shaped by who you are"
videoid: "TQyJenuX_5I"
---
Wherever you're headed in life, you will get a strong start at Central Alabama Community College. CACC's diverse programs and flexible scheduling provide a world of opportunity for state-of-the-art education and training for a wide range of ages, backgrounds, and life goals, which prepare you for a rewarding future.

Your world is shaped by who you are. Your college years are a primary force in shaping who you will become. Central Alabama Community College offers a wide range of athletics, organizations, arts, activities, and events - experiences designed to help you define who you are and decide where you want to go.

The focus of your college experience will, of course, revolve around strong academics and the unparalleled access to state-of-the-art technology, highly qualified faculty and the personal attention afforded by CACC's intimate class sizes. But the growth that comes from discovering new interests, making new friends and exposing yourself to new experiences is equally important in unlocking your full potential.