---
title: "History"
date: 2018-02-06T11:07:05-06:00
draft: false
menu: "about"
featured: true
image: "/images/students.jpg"
subtitle: "Central Alabama Community College has a strong history as one of Alabama’s five original community colleges"
videoid: ""
---

Central Alabama Community College was created by action of the Alabama State Board of Education on February 23, 1989. The board action consolidated Alexander City State Junior College (ACSJC) and Nunnelley State Technical College (NSTC).

Prior to consolidation, the State Legislature’s approval of Act No. 93 on May 3, 1963, established Alexander City State Junior College. While many organizations and individuals were responsible for the College being located in Alexander City, Russell Mills, Inc. donated the property site valued at $750,000. The first classes were held on September 30, 1965, in the old Russell Hospital with an opening enrollment of 442 freshmen.

In September of 1966, ACSJC was moved to its permanent location on Cherokee Road. On October 23, 1966, Governor George Corley Wallace delivered the address at the formal dedication of the College. Alexander City State Junior College was accredited by the Southern Association of Colleges and Schools in December of 1969. A library was completed in January of 1969 and a Health, Education, and Arts complex in January of 1971. A major addition to the HEA Complex was the Wellness Center in February of 1989, which was renovated in 2000. Classes in the Betty Carol Graham Technology Center began fall semester of 2004. A new Learning Resource Center was completed and opened on the Alexander City campus in the Fall of 2013 replacing the John D. Russell Library.

Nunnelley State Technical College in Childersburg was also a direct result of Act No. 93. Along with Congressman William F. (Bill) Nichols, many organizations, individuals, and governing bodies joined together to bring the College to the area. The City of Childersburg contributed some $24,000 for the purchase of twenty-five acres on Highway 280. The acreage was donated to the State for the College site.

The College opened an Instructional Site in Talladega in the Spring of 2006. Construction of the technical college was completed in February of 1966.

The College officially opened on March 7, 1966, with an opening enrollment of 35 full-time students. On September 25, 1966, Governor George Corley Wallace delivered the dedicatory address to more than 1,500 attendees. On December 12, 1973, Nunnelley earned Southern Association of Colleges and Schools accreditation.

Sizeable federal grants in 1973, 1977, 1979, and 1985 allowed Nunnelley to expand plant facilities, program offerings, and student services. The present physical plant has more than doubled in size since first opening its doors.

Coosa Valley School of Nursing began as the Sylacauga School of Nursing in 1921 as a hospital diploma program. The school was reorganized in 1951 and continued to operate as a hospital diploma program until 1994, when CVSN introduced an associate degree nursing program. Coosa Valley School of Nursing relocated from Sylacauga to the new Jim Preuitt Nursing and Allied Health Building on the Childersburg campus in January 2001.

A center in Talladega began classes the spring semester of 2006 with an opening enrollment of 130 students. The 28,500 square foot facility is a result of donated land from the city of Talladega. The Center is a combined partnership with a number of state agencies including the Career Link, Employment Services, Vocational Rehabilitation Services, Adult Education, and Veteran Affairs. The College began teaching evening classes at the Millbrook Instructional site, which is currently housed at Stanhope-Elmore High School, in the Spring of 2015.

Today, the Alexander City and Childersburg campuses, as well as the Millbrook Instructional site and Talladega Center, offer resources and expertise that address the education and training needs of Central Alabama.

At the inception of the two-year college system in Alabama, the Alabama State Board of Education functioned as the trustees for the system colleges. In the spring of 2015, the Alabama Legislature established the Alabama Community System Board of Trustees to oversee the system. On May 27, 2015, Governor Robert Bentley swore in the appointed members of the Board before their first official meeting.
