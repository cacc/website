---
title: "Graduation Spring 2018"
date: 2018-04-06T11:07:05-06:00
draft: false
image: "/images/students.jpg"
color: "color-brand-bg"
---
Wherever you’re headed in life, you will get a strong start at Central Alabama Community College. CACC’s diverse programs and flexible scheduling provide a world of opportunity for state-of-the-art education and training for a wide range of ages, backgrounds, and life goals, which prepare you for a rewarding future.