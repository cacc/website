---
title: "Kicking off Career Technical Education"
date: 2018-02-12T11:07:05-06:00
draft: false
image: "images/news/cte-machine-shop.jpg"
color: "color-grey-bg"
---
Kicking off Career Technical Education month with a group of seniors from Tallassee High School touring our Center for Precision Machining today!